# 2018 Software Studio Midterm project 

Firebase URL: <a href="https://midterm-project-cb9e6.firebaseapp.com/index.html">https://midterm-project-cb9e6.firebaseapp.com</a>

GitLab URL: <a href="https://105062314.gitlab.io/Midterm_Project">https://105062314.gitlab.io/Midterm_Project</a>

## About My Project
* Photography Forum(攝影論壇)
* 主要功能
    1. 發布文章（包含標題、圖片、內容、分類）
    2. 全部文章列表
    3. 分類文章列表
    4. 獨立文章頁面
    5. 於各文章下方留言
    6. 使用者頁面
    7. 使用者大頭照
    8. 開發者資訊

* 細部功能
    1. 動態側邊欄列表
    2. 動態首頁封面
    3. **換頁**功能（每頁有9篇貼文，且按照最新順序排序，並使用DOM新增貼文頁碼也會增加）
    4. 塊狀文章列表
    5. 文章**搜索**
    6. 文章列表之標題連結可進入文章頁面、帳戶連結則可進入不同帳戶之資料
    7. 獨立文章頁面包含**使用者名稱、標題、圖片、內容、時間**
    8. 留言可顯示留言帳戶之大頭貼照、留言時間
    9. 可**刪除**貼文及留言
    10. 於個人頁面顯示**個人資訊及歷史發文**
    11. **更改大頭貼照** 
    12. 文章尚未跑完時，顯示css動畫
    13. 登入介面背景顯示css動畫

## Project request
### Basic components
* Membership Mechanism (20%) : **check**
    - using email Sign Up & Google Sign Up
* Host on your GitLab page (5%) : **check**
* Database read/write (15%) : **check**
    - using firebase
* RWD (15%) : **check**

### Topic Key Functions 
* Forum(15%) : **check**
    - user page, post page, post list page, leave comment under any post

### Advanced Components
* Sign Up/In with Google or other third-party accounts (2.5%) : **check**
* Add Chrome notification (5%) : **check** 
    - When someone leave a comment under your post, you will get notification
* Use CSS animation (2.5%) : **check**
    - loading animation
* Proove your website has strong security (write in your report) (5%) : **check**
    - write down below
* Other functions not included in key functions (1~10%) : **check**
    - check the information above

## Function Detail

### User 
> 1. using firebase **auth()** function 
>    * 利用firebase驗證部分的功能，將使用者資訊存入Authentication，創建uid
>    * 登入方式有 email 及 google 帳號
> 2. 使用者資訊
>    * 我在使用者初次登入時，在database創建一個使用者資訊，將資料存入
>    * 並預設使用者名稱為@前的名稱
> 3. 使用者大頭貼
>    * 當使用者上傳個人頭像後，我在storage創建一個頭貼資料夾，將圖片檔名改為使用者帳號
>    * 在更新大頭貼後，因檔名相同，故會覆蓋掉，達到更新頭貼，也不佔用多餘空間

### Post & Comment (Database)
>    1. using **firebase.database().ref()** funciton
>        * 利用database的功能，將我的貼文資料都push進入database，需要時再從中取出
>    2. **once()** and **on()**
>        * once 是只監聽一次 而on則是持續監聽
>        * 故利用once將原所有資料顯示出來（for each），利用on顯示新增的貼文
>    3. **orderByChild()** 、 **startAt()** and **limitToFirst()**
>        * 此三個function是我用來按照我需要的方式排序database的資料
>        * **orderByChild()** 控制依照什麼排序
>        * **startAt()** 設定開始的index或key
>        * **limitToFirst()** 則是決定幾筆資料
>    4. 分類、使用者歷史貼文與搜尋
>        * 實作方式如上述，在其中加入if條件判斷，即可控制需要顯示的資料
>    5. 留言部分
>        * 方法與貼文相同，只是不需特別排序
>        * 在留言者的大頭照，是先從storage取得圖片，並拿到url，再顯示出來
>    6. 獨立貼文頁面
>        * 使用Query String（在網址後加上'？'，其後的字串即可用來做判斷或搜索用途）技巧
>        * 在載入文章列表時，我已將加上判斷字串後的連結完成
>        * 點擊後，利用當下的url '?'後方的字串來判定我的頁面需要跑哪筆資料，並從database中取出
>        * 利用DOM方式完成，即可動態改變各個獨立貼文頁面
>    7. 個人頁面做法與上相同
>    8. 刪除貼文
>        * 先確認登入者是否為發文者，是則可刪除，否則不可刪除
>        * 利用Query String的字串，尋找此篇貼文的發文者，並比較當前登入的使用者是否相同

### Storage
>1. using **firebase.storage().ref()** function
>    * 利用storage的功能，將圖片上傳firebase
>    * 缺點：速度較慢
>2. **getDownloadURL()**
>    * 可以取得圖片連結使用

## Notification
>1. 文章有新留言時，在發文者頁面顯示通知
>2. 發文者自己留言則不會顯示通知

## Security
>1. 在rules中，加入**auth != null**，讓未登入者無法直接看到網站內容
>2. 在刪除文章時，確認是否為發文者本人，否則無法刪除


## Reference

* <a href="https://www.w3schools.com/">w3school</a>
* <a href="http://sj82516-blog.logdown.com/posts/1048782/auth-firebase-web-operations-validation-review">YJ BLOG firebase教學</a>
* <a href="https://fontawesome.com/">fontawesome</a>
* <a href="https://templated.co/">templated</a>
* <a href="https://getbootstrap.com/">bootstrap</a>