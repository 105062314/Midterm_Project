var open_click = 0;
function openNav() {
    if(open_click == 0){
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        open_click = 1;
    }
    else closeNav();
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";
    open_click = 0;
}
// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.'); 
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

function notifyMe() {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification('Notification title', {
      icon: 'img/pic2-01.png',
      body: "Hey there! You've been notified!",
    });
    // notification.onclick = function () {
    //   window.open("#"); 
    // };
  }
}
var page_now;
var img_url = '';
function page(page_index){
    //running aninmation for waiting
    var temp = [];
    temp[temp.length] = '<div class="fa-3x" ><i class="fas fa-spinner fa-spin"></i></div>';
    document.getElementById('posts').innerHTML = temp.join('');

    page_now = page_index;
    var postsRef = firebase.database().ref('com_list').orderByChild('sort_index');
    // List for store posts html
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    var count_for_page = 0;
    var temp_index ='';
    var temp_sort = 0;
    //my create post

    var posts = [];

    var str_before_date = '<article class="masonry__brick entry format-standard" data-aos="fade-up"><div class="entry__thumb"><a href="single-standard.html" class="entry__thumb-link"><img src="images/thumbs/masonry/lamp-400.jpg" srcset="images/thumbs/masonry/lamp-400.jpg 1x, images/thumbs/masonry/lamp-800.jpg 2x" alt=""></a></div><div class="entry__text"><div class="entry__header"><div class="entry__date"><p>';
    var str_before_title = '<h1 class="entry__title">';
    var str_before_date_type2 = '<article class="masonry__brick entry format-quote" data-aos="fade-up"><div class="entry__thumb"><p><br></p><div class="entry__header"><div class="entry__date"><p>';
    var str_before_title_type2 = '<h1 class="entry__title">';
    postsRef.once('value').then(function (childshot) {
        childshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();
            count_for_page +=1;
            if(count_for_page==(page_index-1)*9+1){
                temp_index = childSnapshot.key;
                temp_sort = childData.sort_index;
            }
        });
        if(count_for_page>=(page_index-1)*9+1){
            var Ref = firebase.database().ref('com_list').orderByChild('sort_index').startAt(temp_sort,temp_index).limitToFirst(9);
            Ref.once('value').then(function (childshot) {
                //var str_img ='';
                childshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    //find img url
                    var storageRef = firebase.storage().ref('post_picture/' + childData.email + '/');
                    var pathReference = storageRef.child(childData.photo_name);
                    pathReference.getDownloadURL().then(function(url) {
                        var img = document.getElementById(childData.photo_name);
                        console.log(img);
                        img.src = url;
                    });
                    posts[posts.length]= str_before_date_type2+childData.time+'</p></div>'+str_before_title_type2+"<a href='posts.html"+'?'+childSnapshot.key +" 'onclick='post_page()'>"+childData.title+'</a></h1></div>'+'<img id="'+childData.photo_name +'" style="width:70%;height:70%">'+'<div class="entry__excerpt"><p>'+childData.data+'</p></div><div class="entry__meta"><span class="entry__meta-links"><a href="profile.html'+'?'+childData.post_name+'">'+childData.email+'</a></span></div></div></article>';
                });
                document.getElementById('posts').innerHTML = posts.join('');
            });
        }
        var pages = [];
        var str_before_page = '<li class="page-item"><a class="page-link" href="#" onclick="page(';
        var str_after_page = '</a></li>';
        var prev = (page_now>1)?page_now-1:1;
        var next = (page_now<=(count_for_page-1)/9)?page_now+1:page_now;
        console.log(count_for_page);
        pages[0] = str_before_page+prev+')">'+'Prev'+str_after_page;
        for(i=1;i<=(count_for_page-1)/9+1;i++){
            pages[pages.length] = str_before_page +i+')">'+ i+str_after_page;
        }
        pages[pages.length] = str_before_page+next+')">'+'Next'+str_after_page;
        document.getElementById('page_index_button').innerHTML = pages.join('');
    });
}
var file_name = '';
function PostaNewArticle(){
    var post_area = [];
    var post_class_type = '<li class="nav-item dropdown"><select id="post_type_seletion"><option value="photos">Photos</option><option value="information">Information</option><option value="video">Video</option><option value="others">Others</option></select></li>';
    post_area[post_area.length] = '<div class="my-3 p-3 bg-white rounded box-shadow"><h5 class="border-bottom border-gray pb-2 mb-0">New Post</h5><textarea class="form-control" rows="1" id="title" style="resize:none"></textarea>'+post_class_type+'<div id="upload_photo_btn"></div>'+'<textarea class="form-control" rows="5" id="comment" style="resize:none"></textarea><div class="media text-muted pt-3"><button id="post_btn" type="button" class="btn btn-success">Submit</button></div></div>';
    document.getElementById('post_area').innerHTML = post_area.join('');
    var post_type_seletion = document.getElementById('post_type_seletion');
    console.log(post_type_seletion.value);


    document.getElementById('page_index_button').innerHTML = "";
    document.getElementById('posts').innerHTML = "";
    post_btn = document.getElementById('post_btn');
    post_title = document.getElementById('title');
    post_txt = document.getElementById('comment');
    
    var user_email = '';
    var user_name = '';
    //var file_name = '';
    firebase.auth().onAuthStateChanged(function (user) {
        // Check user login
        if (user) {
            upload_photo(user);
            var userRef = firebase.database().ref('user_list/'+user.uid);
            userRef.once('value').then(function (childshot){
                var userData = childshot.val();
                user_email = userData.email;
                user_name = userData.user_name;
                console.log(user_name);
            });
        }
    });
    console.log(user_name);
    post_btn.addEventListener('click', function () {
        if(post_title.value ==="" || post_txt.value===""){
            alert("Please text some title and comment!!!");
        }
        else{
            var currentDate = new Date(),
                day = currentDate.getDate(),
                month = currentDate.getMonth() + 1,
                year = currentDate.getFullYear();
            var currentTime = new Date(),
                hours = currentTime.getHours(),
                minutes = currentTime.getMinutes();
            var sortDate = Date.now();
          
              if (minutes < 10) {
                  minutes = "0" + minutes;
              }
            if(file_name==null){
                file_name = ' ';
            }
            var Ref = firebase.database().ref('com_list');
            var data = {
                sort_index: 0-sortDate,
                title: post_title.value,
                post_type_seletion:post_type_seletion.value,
                data: post_txt.value,
                email: user_email,
                post_name : user_name,
                time: year +'/'+month+'/'+day,
                photo_name : file_name
            };
            Ref.push(data);
            post_title.value = "";
            post_txt.value = "";
            init();
        }
    });
}
//save user information into firebase
function create_user_list(user){
    var Ref = firebase.database().ref('user_list/'+user.uid);
    Ref.once('value').then(function (childshot){
        var userData = childshot.val();
        var default_user_name = user.email.split('@');
        var data = {
            email: user.email,
            user_name: default_user_name[0]
            //member_since :
            //post_count:
        };
        console.log(default_user_name[0]);
        if(userData==null){
            Ref.set(data);
            console.log('success');
        }
    });
}
function search_for_post(){
    document.getElementById('posts').innerHTML = "";
    document.getElementById('page_index_button').innerHTML = "";
    var posts = [];
    var search_index = document.getElementById('search').value;
    console.log(search_index);

    var str_before_date = '<article class="masonry__brick entry format-standard" data-aos="fade-up"><div class="entry__thumb"><a href="single-standard.html" class="entry__thumb-link"><img src="images/thumbs/masonry/lamp-400.jpg" srcset="images/thumbs/masonry/lamp-400.jpg 1x, images/thumbs/masonry/lamp-800.jpg 2x" alt=""></a></div><div class="entry__text"><div class="entry__header"><div class="entry__date"><p>';
    var str_before_title = '<h1 class="entry__title">';
    var str_before_date_type2 = '<article class="masonry__brick entry format-quote" data-aos="fade-up"><div class="entry__thumb"><p><br></p><div class="entry__header"><div class="entry__date"><p>';
    var str_before_title_type2 = '<h1 class="entry__title">'

    var Ref = firebase.database().ref('com_list');
        Ref.once('value').then(function (childshot) {
            childshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                console.log(search_index);
                console.log(childData.title);
                if(childData.title.match(search_index)!=null){
                    var storageRef = firebase.storage().ref('post_picture/' + childData.email + '/');
                    var pathReference = storageRef.child(childData.photo_name);
                    pathReference.getDownloadURL().then(function(url) {
                        var img = document.getElementById(childData.photo_name);
                        console.log(img);
                        img.src = url;
                    });
                    posts[posts.length]= str_before_date_type2+childData.time+'</p></div>'+str_before_title_type2+"<a href='posts.html"+'?'+childSnapshot.key +" 'onclick='post_page()'>"+childData.title+'</a></h1></div>'+'<img id="'+childData.photo_name +'" style="width:70%;height:70%">'+'<div class="entry__excerpt"><p>'+childData.data+'</p></div><div class="entry__meta"><span class="entry__meta-links"><a href="profile.html'+'?'+childData.post_name+'">'+childData.email+'</a></span></div></div></article>';
                }
            });
            posts.reverse();
            document.getElementById('posts').innerHTML = posts.join('');
    });
}
// //upload a new photo
function upload_photo(user){

    document.getElementById('upload_photo_btn').innerHTML = "<label class='btn btn-default'>Browse<input id = 'uploadinput' type='file'hidden></label>";
    var uploadFileInput = document.getElementById("uploadinput");
    var storageRef = firebase.storage().ref('post_picture/' + user.email + '/');
    console.log(user.email);
    uploadFileInput.addEventListener("change", function(){
        var file = this.files[0];
        file_name = file.name;
        console.log(file_name);
        var uploadTask = storageRef.child(file.name).put(file);
        uploadTask.on('state_changed', function(snapshot){
            // 觀察狀態變化，例如：progress, pause, and resume

            // 取得檔案上傳狀態，並用數字顯示
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'

                console.log('Upload is paused');
                break;
                case firebase.storage.TaskState.RUNNING: // or 'running'

                console.log('Upload is running');
                break;
            }
        }, function(error) {
            console.log("Error!");
        }, function() {
        // Handle successful uploads on complete
        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
        downloadURL = uploadTask.snapshot.downloadURL;
        });
    },false);
    console.log(file_name);
    //return file_name;
}
//load different class
function load_class(class_index){
    document.getElementById('posts').innerHTML = "";
    document.getElementById('page_index_button').innerHTML = "";
    var posts = [];

    console.log(class_index);

    var str_before_date = '<article class="masonry__brick entry format-standard" data-aos="fade-up"><div class="entry__thumb"><a href="single-standard.html" class="entry__thumb-link"><img src="images/thumbs/masonry/lamp-400.jpg" srcset="images/thumbs/masonry/lamp-400.jpg 1x, images/thumbs/masonry/lamp-800.jpg 2x" alt=""></a></div><div class="entry__text"><div class="entry__header"><div class="entry__date"><p>';
    var str_before_title = '<h1 class="entry__title">';
    var str_before_date_type2 = '<article class="masonry__brick entry format-quote" data-aos="fade-up"><div class="entry__thumb"><p><br></p><div class="entry__header"><div class="entry__date"><p>';
    var str_before_title_type2 = '<h1 class="entry__title">'

    var Ref = firebase.database().ref('com_list');
        Ref.once('value').then(function (childshot) {
            childshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                if(childData.post_type_seletion==class_index){
                    var storageRef = firebase.storage().ref('post_picture/' + childData.email + '/');
                    var pathReference = storageRef.child(childData.photo_name);
                    pathReference.getDownloadURL().then(function(url) {
                        var img = document.getElementById(childData.photo_name);
                        console.log(img);
                        img.src = url;
                    });
                    posts[posts.length]= str_before_date_type2+childData.time+'</p></div>'+str_before_title_type2+"<a href='posts.html"+'?'+childSnapshot.key +" 'onclick='post_page()'>"+childData.title+'</a></h1></div>'+'<img id="'+childData.photo_name +'" style="width:70%;height:70%">'+'<div class="entry__excerpt"><p>'+childData.data+'</p></div><div class="entry__meta"><span class="entry__meta-links"><a href="profile.html'+'?'+childData.post_name+'">'+childData.email+'</a></span></div></div></article>';
                }
            });
            posts.reverse();
            document.getElementById('posts').innerHTML = posts.join('');
    });
}

function init() {
    //running aninmation for waiting
    var temp = [];
    temp[temp.length] = '<div class="fa-3x"><i class="fas fa-spinner fa-spin"></i></div>';
    document.getElementById('posts').innerHTML = temp.join('');
    document.getElementById('post_area').innerHTML = "";

    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) { 
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            create_user_list(user);
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                var check = confirm("Sure to logout?");
                if(check){
                    window.location.reload();
                    firebase.auth().signOut().then(function(e)
                    {
                        create_alert("success","sucess");
                        alert("sucess");
                        window.location.reload();
                    }).catch(function(e)
                    {
                    create_alert("error",e.message);
                    });;
                }
              })
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            var space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            document.getElementById('posts').innerHTML = "<div style='margin:0 auto;'><h3>"+space+space+space+space+"Please Sign in to see more...</h3></div>";
        }
    });
    page(1);
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}
window.onload = function () {
    init();
};