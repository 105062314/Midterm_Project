var open_click = 0;
function openNav() {
    if(open_click == 0){
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        open_click = 1;
    }
    else closeNav();
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";
    open_click = 0;
}
// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.'); 
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

function notifyMe() {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification('Comment', {
      icon: 'pic2-01.png',
      body: "Someone leave a comment below your post!",
    });
    notification.onclick = function () {
      window.open("#");
    };
  }
}
// //upload a new photo
function upload_photo(user){

    document.getElementById('upload_photo_btn').innerHTML = "<label class='btn btn-default'>Edit Photo<input id = 'uploadinput' type='file'hidden></label>";
    var uploadFileInput = document.getElementById("uploadinput");
    var storageRef = firebase.storage().ref('profile_picture/' + user.email + '/');
    //console.log(user.email);
    uploadFileInput.addEventListener("change", function(){
        var file = this.files[0];
        var uploadTask = storageRef.child(user.email+".png").put(file);
        uploadTask.on('state_changed', function(snapshot){
            // 觀察狀態變化，例如：progress, pause, and resume

            // 取得檔案上傳狀態，並用數字顯示
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            e.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'

                console.log('Upload is paused');
                break;
                case firebase.storage.TaskState.RUNNING: // or 'running'

                console.log('Upload is running');
                break;
            }
        }, function(error) {
            console.log("Error!");
        }, function() {
        // Handle successful uploads on complete
        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
            downloadURL = uploadTask.snapshot.downloadURL;
        });
    },false);
}
function get_img(email){
    //var getPicBtn = document.getElementById("getPicBtn");
    //var downloadedImg = document.getElementById("downloadedImg");
    var storageRef = firebase.storage().ref('profile_picture/' + email + '/');
    var image_url ='';
    //getPicBtn.addEventListener("click", function(){
    var pathReference = storageRef.child(email+'.png');
    document.getElementById("downloadedImg").innerHTML = "<br><img src='img/person.jpg'  class='rounded-circle' style='width:200px;height:200px;display:block; margin:auto; overflow:hidden;' ></div>";
    pathReference.getDownloadURL().then(function(url) {
        var temp = [];
        temp[temp.length] = '<div class="fa-3x"><i class="fas fa-spinner fa-spin"></i></div>';
        document.getElementById('downloadedImg').innerHTML = temp.join('');
        console.log(url);
        image_url = url;
        document.getElementById("downloadedImg").innerHTML = "<br><img src="+image_url+"  class='rounded-circle' style='width:200px;height:200px;display:block; margin:auto; overflow:hidden;' ></div>";
    })
}

function init() {
    //running aninmation for waiting
    // var temp = [];
    // temp[temp.length] = '<div class="fa-3x"><i class="fas fa-spinner fa-spin"></i></div>';
    // document.getElementById('profile').innerHTML = temp.join('');
    
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        console.log(user.email);
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            //get_img(user);
            upload_photo(user);
            user_email = user.email;
            console.log(user.email)
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                var check = confirm("Sure to logout?");
                if(check){
                    window.location.reload();
                    firebase.auth().signOut().then(function(e)
                    {
                        create_alert("success","sucess");
                        alert("sucess");
                    }).catch(function(e)
                    {
                    create_alert("error",e.message);
                    });;
                }
              })
            } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('profile').innerHTML = "";
        }
    });

    //find the specific user 
    var url = window.location.href;
    var url_index = url.split('?');
    var index_post = [];
    //console.log(url_index[1]);
    
    //if user click my page, run this
    if(url_index[1]==null){
        firebase.auth().onAuthStateChanged(function (user) {
            // Check user login
            //var photo_url = '<br><img src="images/avatars/user-02.jpg" style="width:20%;display:block; margin:auto;border-radius:50%;">';
            if (user){
                var userRef = firebase.database().ref('user_list/'+user.uid);
                userRef.once('value').then(function (childshot) {
                    get_img(user.email);
                    var childData = childshot.val();
                    //console.log(childData.email);
                    index_post[index_post.length]="<div style='text-align:center'><h1>"+ childData.user_name+"</h1><br>"+childData.email+"</div>";
                    // index_post[index_post.length]="<pre><div>" + childData.email +"<br>"+"<h3><b>"+ childData.title +"</b></h3>"+ childData.data +"<br>"+"<br>"+childData.time+ "</div></pre>";
                    document.getElementById('profile').innerHTML = index_post.join('');
                });
                var posts = [];
                var str_before_date = '<article class="masonry__brick entry format-standard" data-aos="fade-up"><div class="entry__thumb"><a href="single-standard.html" class="entry__thumb-link"><img src="images/thumbs/masonry/lamp-400.jpg" srcset="images/thumbs/masonry/lamp-400.jpg 1x, images/thumbs/masonry/lamp-800.jpg 2x" alt=""></a></div><div class="entry__text"><div class="entry__header"><div class="entry__date"><p>';
                var str_before_title = '<h1 class="entry__title">';
                var str_before_date_type2 = '<article class="masonry__brick entry format-quote" data-aos="fade-up"><div class="entry__thumb"><p><br></p><div class="entry__header"><div class="entry__date"><p>';
                var str_before_title_type2 = '<h1 class="entry__title">'

                var Ref = firebase.database().ref('com_list');
                Ref.once('value').then(function (childshot) {
                    childshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        if(childData.email==user.email){
                            var storageRef = firebase.storage().ref('post_picture/' + childData.email + '/');
                            var pathReference = storageRef.child(childData.photo_name);
                            pathReference.getDownloadURL().then(function(url) {
                                var img = document.getElementById(childData.photo_name);
                                //console.log(img);
                                img.src = url;
                            });
                            posts[posts.length]= str_before_date_type2+childData.time+'</p></div>'+str_before_title_type2+"<a href='posts.html"+'?'+childSnapshot.key +" 'onclick='post_page()'>"+childData.title+'</a></h1></div>'+'<img id="'+childData.photo_name +'" style="width:70%;height:70%">'+'<div class="entry__excerpt"><p>'+childData.data+'</p></div><div class="entry__meta"><span class="entry__meta-links"><a href="profile.html'+'?'+childData.post_name+'">'+childData.email+'</a></span></div></div></article>';
                        }
                    });
                    posts.reverse();
                    document.getElementById('posts').innerHTML = posts.join('');
                });
            }
        });
    }
    //if you want to find other user, run this
    else{
        userRef = firebase.database().ref('user_list');
        userRef.once('value').then(function (childshot) {
            childshot.forEach(function (childSnapshot) {
                //console.log(childSnapshot.key);
                var childData = childSnapshot.val();
                if(url_index[1]==childData.user_name){
                    get_img(childData.email);
                    index_post[index_post.length]="<div style='text-align:center'><h1>"+ childData.user_name+"</h1><br>"+childData.email+"</div>";
                    document.getElementById('profile').innerHTML = index_post.join('');
                    var posts = [];
                    var str_before_date = '<article class="masonry__brick entry format-standard" data-aos="fade-up"><div class="entry__thumb"><a href="single-standard.html" class="entry__thumb-link"><img src="images/thumbs/masonry/lamp-400.jpg" srcset="images/thumbs/masonry/lamp-400.jpg 1x, images/thumbs/masonry/lamp-800.jpg 2x" alt=""></a></div><div class="entry__text"><div class="entry__header"><div class="entry__date"><p>';
                    var str_before_title = '<h1 class="entry__title">';
                    var str_before_date_type2 = '<article class="masonry__brick entry format-quote" data-aos="fade-up"><div class="entry__thumb"><p><br></p><div class="entry__header"><div class="entry__date"><p>';
                    var str_before_title_type2 = '<h1 class="entry__title">'
                    var Ref = firebase.database().ref('com_list');
                    Ref.once('value').then(function (childshot_1) {
                        childshot_1.forEach(function (childSnapshot_1) {
                            var comDate = childSnapshot_1.val();
                            if(comDate.email===childData.email){
                                var storageRef = firebase.storage().ref('post_picture/' + childData.email + '/');
                                var pathReference = storageRef.child(comDate.photo_name);
                                pathReference.getDownloadURL().then(function(url) {
                                    var img = document.getElementById(comDate.photo_name);
                                    //console.log(img);
                                    img.src = url;
                                });
                                posts[posts.length]= str_before_date_type2+comDate.time+'</p></div>'+str_before_title_type2+"<a href='posts.html"+'?'+childSnapshot_1.key +" 'onclick='post_page()'>"+comDate.title+'</a></h1></div>'+'<img id="'+comDate.photo_name +'" style="width:70%;height:70%">'+'<div class="entry__excerpt"><p>'+comDate.data+'</p></div><div class="entry__meta"><span class="entry__meta-links"><a href="profile.html'+'?'+comDate.post_name+'">'+comDate.email+'</a></span></div></div></article>';
                            }
                        });
                        posts.reverse();
                        document.getElementById('posts').innerHTML = posts.join('');
                    });
                }
                
            });
        });
    }
}

window.onload = function () {
    init();
};