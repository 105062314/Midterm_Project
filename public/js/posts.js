var open_click = 0;
function openNav() {
    if(open_click == 0){
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
        document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
        open_click = 1;
    }
    else closeNav();
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    document.body.style.backgroundColor = "white";
    open_click = 0;
}
// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
    alert('Desktop notifications not available in your browser. Try Chromium.'); 
    return;
  }

  if (Notification.permission !== "granted")
    Notification.requestPermission();
});

function notifyMe() {
  if (Notification.permission !== "granted")
    Notification.requestPermission();
  else {
    var notification = new Notification('Comment', {
      icon: 'pic2-01.png',
      body: "Someone leave a comment below your post!",
    });
    notification.onclick = function () {
      window.open("#");
    };
  }
}
function get_img(email,photo_name){
    //console.log(email);
    if(photo_name==''){
        document.getElementById("downloadedImg").innerHTML ="<button class='button_remove' id='button_remove' style='vertical-align:middle' onclick='remove_post();'>Remove</button>";
        return;
    }
    var storageRef = firebase.storage().ref('post_picture/' + email + '/');
    var image_url ='';
    var pathReference = storageRef.child(photo_name);
    pathReference.getDownloadURL().then(function(url) {
        //console.log(url);
        image_url = url;
        document.getElementById("downloadedImg").innerHTML = "<button class='button_remove' id='button_remove' style='vertical-align:middle' onclick='remove_post();'>Remove</button><br><img src="+image_url+"  style='width:40%;display:block; margin:auto;' ></div>";
    })
}
function remove_post(){
    firebase.auth().onAuthStateChanged(function (user) {
        var url = window.location.href;
        var url_index = url.split('?');
        firebase.database().ref('com_list/'+url_index[1]).once('value').then(function(snapshot) {
            console.log(snapshot.val().email);
            if(snapshot.val().email==user.email){
                var check = confirm("Sure to remove?")
                if(check){
                    var url = window.location.href;
                    var url_index = url.split('?');
                    console.log(url_index[1]);
                    firebase.database().ref('com_list/'+url_index[1]).remove();
    
                    var commentRef = firebase.database().ref('comment_list');
                    commentRef.once('value').then(function (snapshot) {
                        snapshot.forEach(function (childSnapshot) {
                            var childData = childSnapshot.val();
                            if(childData.index===url_index[1]){
                                console.log(childSnapshot.key);
                                firebase.database().ref('comment_list/'+childSnapshot.key).remove();
                            }
                        });
                    });
                    console.log("remove success");
                    window.location.reload();
                }
            }
            else{
                alert("You are not the poster!");
            }
        });
    });
}

function init() {
    //running aninmation for waiting
    var temp = [];
    temp[temp.length] = '<div class="fa-3x"><i class="fas fa-spinner fa-spin"></i></div>';
    document.getElementById('post').innerHTML = temp.join('');
    
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                var check = confirm("Sure to logout?");
                if(check){
                    window.location.reload();
                    firebase.auth().signOut().then(function(e)
                    {
                        create_alert("success","sucess");
                        alert("sucess");
                        window.location.reload();
                    }).catch(function(e)
                    {
                    create_alert("error",e.message);
                    });;
                }
            })
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post').innerHTML = "";
        }
    });

    //find the index post title and content
    var url = window.location.href;
    var url_index = url.split('?');
    var index_post = [];
    
    var postsRef = firebase.database().ref('com_list');
    postsRef.once('value').then(function (childshot) {
            childshot.forEach(function (childSnapshot) {
                if(childSnapshot.key===url_index[1]){
                    var childData = childSnapshot.val();
                    console.log(childSnapshot.key);
                    console.log(childData.photo_name);
                    get_img(childData.email,childData.photo_name);
                    index_post[index_post.length]="<pre><div>" + childData.email +"<br>"+"<h3><b>"+ childData.title +"</b></h3>"+ childData.data +"<br>"+"<br>"+childData.time+ "</div></pre>";
                }
        });
        document.getElementById('post').innerHTML = index_post.join('');
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('commentText');

    post_btn.addEventListener('click', function () {
        if(post_txt.value===""){
            alert("Please text some comment!!!");
        }
        else{

            var currentDate = new Date(),
                day = currentDate.getDate(),
                month = currentDate.getMonth() + 1,
                year = currentDate.getFullYear();
            var currentTime = new Date(),
                hours = currentTime.getHours(),
                minutes = currentTime.getMinutes();
          
              if (minutes < 10) {
                  minutes = "0" + minutes;
              }
              
            var Ref = firebase.database().ref('comment_list');
            var data = {
                index: url_index[1],
                data: post_txt.value,
                email: user_email,
                time: year +'/'+month+'/'+day,
                sort_index: currentDate 
            };
            Ref.push(data);
            post_txt.value = "";
        }
    });


    // The html code for comment
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
    var str_mid_username = "</h6><div class='media text-muted pt-3'>";
    //<img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'>
    var str_mid_username_2 =  "<strong class='d-block text-black'>";
    var str_after_content = "</div></div>\n";

    var commentRef = firebase.database().ref('comment_list');
    // List for store posts html
    var total_comment = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    //post list (comment)
    commentRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                if(childData.index===url_index[1]){
                    total_comment[total_comment.length] = str_before_username +childData.email + str_mid_username+'<img src="img/person.jpg" id="'+childSnapshot.key +'"  alt="" class="mr-2 rounded" style="height:32px; width:32px;">'+str_mid_username_2 + "</strong>" + childData.data + " " +childData.time+ str_after_content+'</div>';
                    var storageRef = firebase.storage().ref('profile_picture/' + childData.email + '/');
                    var pathReference = storageRef.child(childData.email+".png");
                    pathReference.getDownloadURL().then(function(url) {
                        var img = document.getElementById(childSnapshot.key);
                        console.log(img);
                        img.src = url;
                        total_comment[total_comment.length] = str_before_username +childData.email + str_mid_username+'<img id="'+childSnapshot.key +'"  alt="" class="mr-2 rounded" style="height:32px; width:32px;">'+str_mid_username_2 + "</strong>" + childData.data + " " +childData.time+ str_after_content+'</div>';
                    });
                }
                first_count += 1;
            });
            document.getElementById('comment').innerHTML = total_comment.join('');

            commentRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var postsRef = firebase.database().ref('com_list');
                    postsRef.once('value').then(function (childshot) {
                        childshot.forEach(function (childSnapshot) {
                            var childpost = childSnapshot.val();
                            if(childSnapshot.key===url_index[1]){
                                if(childpost.email==user_email&&childpost.email!=childData.email)
                                    notifyMe();
                            }
                        });
                    });
                    
                    if(childData.index===url_index[1]){
                        total_comment[total_comment.length] = str_before_username +childData.email + str_mid_username+'<img src="img/person.jpg"  alt="" class="mr-2 rounded" style="height:32px; width:32px;">'+str_mid_username_2 + "</strong>" + childData.data + " " +childData.time+ str_after_content+'</div>';
                        var storageRef = firebase.storage().ref('profile_picture/' + childData.email + '/');
                        var pathReference = storageRef.child(childData.email+".png");
                        pathReference.getDownloadURL().then(function(url) {
                            var img = document.getElementById(data.key);
                            console.log(img);
                            img.src = url;
                            total_comment[total_comment.length] = str_before_username +childData.email + str_mid_username+'<img id="'+data.key +'"  alt="" class="mr-2 rounded" style="height:32px; width:32px;">'+str_mid_username_2 + "</strong>" + childData.data + " " +childData.time+ str_after_content+'</div>';
                        });
                        document.getElementById('comment').innerHTML = total_comment.join('');
                        window.location.reload();
                    }
                }
            });
        })
        .catch(e => console.log(e.message));

}

window.onload = function () {
    init();
};